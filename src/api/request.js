import axios from 'axios'
import { getCookie } from '@/utils/cookie.js'

export const baseUrl =
  process.env.NODE_ENV === 'production'
    ? 'http://admin.oaloft.com'
    : 'http://0.0.0.0:5000'


const request = axios.create({
  baseURL: baseUrl + '/api/v1/app',
  timeout: 6000,
})

/**
 * 拦截请求
 */
request.interceptors.request.use(
  request => {
    const token = getCookie()
    if (token) {
      request.headers['TOKEN'] = token
    }
    return request
  },
  error => {
    return Promise.reject(error)
  }
)

/**
 * 拦截响应
 */
request.interceptors.response.use(
  response => {
    if (response.data.status === 200) {
      return response.data
    } else {
      return Promise.reject(response.data.errmsg)
    }
  },
  error => {
    return Promise.reject(error)
  }
)

export default request
