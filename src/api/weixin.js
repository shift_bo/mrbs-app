// import wx from 'weixin-js-sdk'
import request, { baseUrl } from './request'
import axios from 'axios'
import wx from 'weixin-js-sdk'
import configSDK from '@/utils'
/**
 * 配置微信jsksdk
 */
const configureWeixin = ({
  appid,
  timestamp,
  noncestr,
  signature,
  jsApiList,
}) => {
  wx.config({
    beta: true,
    debug: true,
    appId: appid,
    timestamp: timestamp,
    nonceStr: noncestr,
    signature: signature,
    jsApiList: jsApiList,
  })

  wx.ready(function() {})

  wx.error(function(res) {})
}

/**
 * 初始化路由的jssdk
 * @param {path} path 页面路由
 */
export const initWeixin = path => {
  let url = baseUrl + '/api/v1/weixin/config'
  axios
    .post(url, {
      path,
    })
    .then(resp => {
      let data = resp.data.data
      data.jsApiList = ['openUserProfile', 'onHistoryBack']
      configureWeixin(data)
    })
}

export const toWeixinBack = () => {
  wx.onHistoryBack(function() {
    return confirm('确定要放弃当前页面的修改？')
  })
}

