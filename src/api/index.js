import request from './request'

export const getCurrentUser = () => {
  return request({
    url: '/currentuser',
  })
}

/**
 * 获取区域列表
 */
export const getAreaList = () => {
  return request({
    url: '/areas',
  })
}

/**
 * 获取区域房间列表
 */
export const getAreaRoomList = ({ name, date }) => {
  return request({
    url: `/areas/rooms?name=${name}&date=${date}`,
  })
}

export const getAreaRoom = ({ roomId, date }) => {
  return request({
    url: `/areas/rooms/${roomId}?date=${date}`,
    method: 'get',
  })
}

/**
 * 获取会议列表
 */
export const getMeetingList = ({ selected = '', page = 1, size = 20 ,mid=''}) => {
  return request({
    url: `/meetings?page=${page}&size=${size}&selected=${selected}&mid=${mid}`,
  })
}

/**
 * 发布会议
 */
export const newMeeting = data => {
  return request({
    url: '/meetings',
    method: 'post',
    data,
  })
}

/**
 * 编辑会议
 */
export const editMeeting = (mid, data) => {
  return request({
    url: `/meetings/${mid}`,
    method: 'post',
    data,
  })
}

export const getMeeting = mid => {
  return request({
    url: `/meetings/${mid}`,
  })
}

export const deleteMeeting = mid => {
  return request({
    url: `/meetings/${mid}`,
    method: 'delete',
  })
}

/**
 * 接受会议邀请
 */
export const acceptMeeting = mid => {
  return request({
    url: `/meetings/${mid}/employee/accept`,
    method: 'post',
  })
}

/**
 * 请假
 */
export const restMeeting = mid => {
  return request({
    url: `/meetings/${mid}/employee/rest`,
    method: 'post',
  })
}

// 获取组织结构
export const getOrgnazation = ({ departmentId = 0 }) => {
  return request({
    url: `/orgnazation/${departmentId}`,
  })
}

/**
 * 获取员工列表
 */
export const searchEmployeeList = name => {
  return request({
    url: `/employees/${name}`,
  })
}

/**
 * 获取序列会议的信息
 */
export const getRepeatInfo = repeatId => {
  return request({
    url: `/repeat/${repeatId}/info`,
  })
}
/*
 * 获取设备列表
 */
export const getDeviceList = () => {
  return request({
    url: '/devices',
  })
}

/**
 * 获取会议预订信息
 */
export const getMeetingInfo = meetingId => {
  return request({
    url: `/meetings/${meetingId}/info`,
  })
}

/**
 * 签到会议
 */

export const signMeeting = data =>{
 return request({
   url : '/meetings/sign',
   method: 'post',
   data,

 })
}

export const getSDKconfig = (url) => {
  return request({
    url: `/v/weixin/sdk?u=${url}`
  })
}

export const giftMeeting = data =>{
  return request({
    url:'/meeting/gift',
    method:'post',
    data
  })
}