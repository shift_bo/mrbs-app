// 会议创建页面和编辑页面的混入
import { mapGetters } from 'vuex'

import RoomItemInfo from '@/views/room/components/RoomItemInfo'
import RoomItemTimeBlock from '@/views/room/components/RoomItemTimeBlock'
import MeetingCalendarItem from '../components/MeetingCalendarItem'
import { getAreaRoom } from '@/api'

export default {
  components: {
    RoomItemInfo,
    RoomItemTimeBlock,
    MeetingCalendarItem,
  },
  data() {
    return {
      room: {}, // 会议室
      date: '',
      isShow: false,
      notice:[
        {
          values:['是','否']
        }
      ],
      repTypeSlots: [
        {
          values: ['不重复', '每周', '每月'],
        },
      ],
      weekSlots: [
        { values: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'] },
      ],
      monthSlots: [
        {
          values: this.$dt.getDayList(),
        },
      ],
      labelWidth: '80px',
      options: [
        {
          label: '企业微信',
          value: '1',
          disabled: 'disabled',
        },
        {
          label: '邮件',
          value: '2',
        },
      ],
      formData: {
        repeatId: '', // 序列会议id
        name: '',
        startTime: '',
        endTime: '',
        description: '',
        noticeType: ['1'],
        org: '',
        repType: '不重复',
        repWeek: '',
        repDay: '',
        endDate: '',
        action: 'check',
        isNotice:'否'
      },
      rules: {
        name: { required: true, message: '主题不能为空' },
        startTime: { required: true, message: '开始时间不能为空' },
        endTime: { required: true, message: '结束时间不能为空' },
        org: { required: true, message: '参会人不能为空' },
        repWeek: {
          message: '重复星期不能为空',
          validator: () => {
            if (!this.isRepeatWeek) {
              return true
            }
            return this.formData.repWeek !== ''
          },
        },
        repDay: {
          message: '重复日期不能为空',
          validator: () => {
            if (!this.isRepeatMonth) {
              return true
            }
            return this.formData.repDay !== ''
          },
        },
        endDate: {
          message: '截止日期不能小于开始日期',
          validator: () => {
            if (this.isRepeat && this.formData.endDate < this.date) {
              return false
            }
            if (this.isRepeat && this.formData.endDate < this.date) {
              this.rules.endDate.message = '截止日期不能多余三个月'
              return false
            }
            return true
          },
        },
      },
    }
  },
  methods: {
    getCurrentRoom() {
      // 获取当前会议室信息
      let query = this.$route.query
      this.date = query.date || this.date
      getAreaRoom({ roomId: query.roomId, date: this.date }).then(resp => {
        if (resp.status === 200) {
          this.room = resp.data.room
        }
      })
    },
    handleTimeBlock(timeBlock) {
      // 处理时间块的开始和结束时间
      if (timeBlock.length > 1) {
        this.formData.startTime = timeBlock[0]
        this.formData.endTime = this.getEndTime(timeBlock[timeBlock.length - 1])
      } else if (timeBlock.length === 1) {
        this.formData.startTime = timeBlock[0]
        let t = timeBlock[0].split(':')
        if(t[1]==='30'){
          this.formData.endTime=String(Number(t[0])+1)+':00'
        }else{
          this.formData.endTime = String(t[0])+':30'
        }
      } else {
        this.formData.startTime = ''
        this.formData.endTime = ''
      }
    },
    getEndTime(time) {
      return this.$dt
        .newDate(this.date + ' ' + time)
        .addMinute(30)
        .getTime()
    },
    showErrorOrSubmit(resp_data) {
      let message = ''
      if (resp_data.isExists) {
        for (let meeting of resp_data.meetingList) {
          let st = this.$dt
            .newDate(meeting.startTime)
            .format('YYYY-MM-DD HH:mm')
          let et = this.$dt.newDate(meeting.endTime).format('HH:mm')
          message += `\n${st} ~ ${et} 与会议: “${meeting.name}” 冲突。`
        }

        this.$messagebox.alert(message, '冲突').then(action => {
          this.$messagebox
            .confirm('是否跳过冲突重新提交', '提示')
            .then(action => {
              if (action === 'confirm') {
                this.formData.action = 'execute'
                this.onSubmit()
              }
            })
        })
      } else {
        this.toLink('/meeting')
      }
    },
  },
  computed: {
    ...mapGetters({
      selectedOrgs: 'selectedOrgs',
    }),
    timeList() {
      // 获取列表
      let startHour = this.room.startHour
      let endHour = this.room.endHour
      let result = []
      for (let i = startHour; i <= endHour; i++) {
        result.push(i + ':00')
        result.push(i + ':30')
      }
      return result
    },
    isRepeat() {
      return this.formData.repType !== '不重复'
    },
    isRepeatMonth() {
      return this.formData.repType === '每月'
    },
    isRepeatWeek() {
      return this.formData.repType === '每周'
    },
  },
  watch: {
    date(val) {
      if (val) {
        let query = this.$route.query
        query.date = val
        getAreaRoom({ roomId: query.roomId, date: val }).then(resp => {
          if (resp.status === 200) {
            this.room = resp.data.room
          }
        })
      }
    },
  },
}
