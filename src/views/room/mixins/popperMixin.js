export default {
  data() {
    return {
      popperValue: {
        owner: '',
        startTime: '',
        endTime: '',
        name: '',
      },
    }
  },
  methods: {
    closePopper(ref, idx) {
      // 关掉popover
      let popper = this.$refs[ref][idx]
      popper.showPopper = false
    },
  },
}
