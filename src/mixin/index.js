import { mapMutations } from 'vuex'

function removeCache(vm) {
  // 删除缓存
  let vnode = vm.$vnode
  if (vnode && vnode.data.keepAlive) {
    if (
      vnode.parent &&
      vnode.parent.componentInstance &&
      vnode.parent.componentInstance.cache
    ) {
      if (vnode.componentOptions) {
        let cache = vnode.parent.componentInstance.cache
        let keys = vnode.parent.componentInstance.keys
        let cacheKey = vnode.componentOptions.Ctor.cid
        let idx = keys.indexOf(cacheKey.toString())
        if (idx > -1 && cache[cacheKey]) {
          keys.splice(idx, 1)
          delete cache[cacheKey]
        }
      }
    }
  }
  vm.$destroy()
}

export default {
  beforeRouteLeave: function(to, from, next) {
    let meta = from.meta
    if (meta.keepAliveRoutes && meta.keepAliveRoutes.includes(to.name)) {
      this.pushRouteStack(this)
    } else if (
      meta.keepAliveRoutes &&
      !meta.keepAliveRoutes.includes(to.name)
    ) {
      removeCache(this)
      this.clearSelectedOrgs()
      this.clearRouteStack()
    }
    next()
  },
  methods: {
    ...mapMutations({
      pushRouteStack: 'PUSH_ROUTE_STACK',
      deleteRouteStack: 'DELETE_ROUTE_STACK',
      clearRouteStack: 'CLEAR_ROUTE_STACK',
      clearSelectedOrgs: 'CLEAR_SELECT_ORG',
    }),
    toLink(link) {
      this.$router.push(link)
    },
    toBack(n = -1) {
      this.$router.go(n)
    },
  },
}
