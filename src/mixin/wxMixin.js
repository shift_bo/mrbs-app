import { initWeixin } from '@/api/weixin'
import { isIPhone } from '@/utils/index'

export default {
  mounted() {
    let path = ''
    if (isIPhone()) {
      path = location.origin
    } else {
      path = location.href.split('#')[0]
    }
    let url = encodeURIComponent(path)
    initWeixin(url)
  },
}
