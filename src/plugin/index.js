import DateTime from '@/utils/datetime'
import Type from '@/utils/type'
import Cache from '@/utils/cache'
import Mixin from '@/mixin'
import ui from '@/components'
import directives from '@/directive'

const install = function(Vue) {
  Vue.component(ui.MCalendar.name, ui.MCalendar)
  Vue.component(ui.MDatetimePicker.name, ui.MDatetimePicker)
  Vue.component(ui.MForm.name, ui.MForm)
  Vue.component(ui.MTab.name, ui.MTab)
  Vue.component(ui.MTabItem.name, ui.MTabItem)
  Vue.component(ui.MCell.name, ui.MCell)
  Vue.component(ui.MSearch.name, ui.MSearch)
  Vue.component(ui.MField.name, ui.MField)
  Vue.component(ui.MCheckList.name, ui.MCheckList)
  Vue.component(ui.MNavbar.name, ui.MNavbar)

  // 全局混入
  Vue.mixin(Mixin)

  // 全局指令
  for (let key in directives) {
    Vue.directive(key, directives[key])
  }

  // 全局方法和实例方法
  Vue.$dt = Vue.prototype.$dt = DateTime
  Vue.$type = Vue.prototype.$type = Type
  Vue.$cache = Vue.prototype.$cache = Cache
}

export default install
