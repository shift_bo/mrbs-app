import router from './routers.js'
import { getCookie } from '@/utils/cookie.js'
import store from '@/store'
import { baseUrl } from '@/api/request'

// 设置企业微信应用标题
const setDocumentTitle = to => {
  const ua = navigator.userAgent.toLowerCase()
  if (ua.indexOf('iphone') > -1 && ua.indexOf('micromessenger') > -1) {
    setTimeout(() => {
      document.title = to.meta.title
      const iframe = document.createElement('iframe')
      iframe.style.visibility = 'hidden'
      iframe.style.width = '1px'
      iframe.style.height = '1px'
      iframe.src = '/favicon.ico' // 这里
      iframe.onload = () => {
        setTimeout(() => {
          document.body.removeChild(iframe)
        }, 0)
      }
      document.body.appendChild(iframe)
    }, 0)
  }
}

// 判断是否从通知页面跳转过来， 如果是则跳转
const toDetailPage = (to, next) => {
  if (to.path === '/room' && to.query.mid !== undefined) {
    next({
      path: '/meeting/detail',
      query: {
        mid: to.query.mid,
      },
    })
  } else {
    next()
  }
}

// 认证
const authenticate = (to,next) => {
  const token = getCookie()
  if (token) {
    if (!store.getters.currentUser) {
      store.dispatch('ActionGetCurrentUser').then(() => {})
    }
  } else {
    if (process.env.NODE_ENV === 'production') {
      if(to.path==='/meeting/sign'){
        const rId = to.query.rid
        window.location.href = baseUrl +'/api/v1/weixin/oauth2/web?rid='+rId
      }else if (to.path==='/meeting/new'){
        const roomId = to.query.roomId
        window.location.href = baseUrl +'/api/v1/weixin/oauth2/web?roomId='+roomId+'&date='+new Date().toLocaleString().split(' ')[0].replace(/\//g,'-')
      }else if(to.path==='/meeting/select'){
        next()
      }else{
        window.location.href = baseUrl + '/api/v1/weixin/oauth2/web'
      }
    }
  }
}

router.beforeEach((to, from, next) => {
  authenticate(to,next)
  setDocumentTitle(to)
  if (!store.getters.currentUser) {
    store.dispatch('ActionGetCurrentUser').then(() => {
      toDetailPage(to, next)
    })
  } else {
    toDetailPage(to, next)
  }
})

export default router
