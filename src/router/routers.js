import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/home/index.vue'

Vue.use(Router)

// 路由默认缓存所有页面， 指定缓存通过keepAliveRoutes控制

const routers = new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/room',
      component: Home,
      children: [
        {
          path: 'room',
          name: 'room',
          component: () => import('@/views/room/index.vue'),
          meta: {
            title: '会议室',
            keepAlive: false,
            keepAliveRoutes: ['meeting-new', 'meeting'],
          },
        },
        {
          path: 'meeting',
          name: 'meeting',
          component: () => import('@/views/meeting/index.vue'),
          meta: {
            title: '会议管理',
          },
        },
      ],
    },
    {
      path: '/meeting/new',
      name: 'meeting-new',
      component: () => import('@/views/meeting/new.vue'),
      meta: {
        title: '新增会议',
        keepAliveRoutes: ['org'],
      },
    },
    {
      path: '/meeting/edit',
      name: 'meeting-edit',
      component: () => import('@/views/meeting/edit.vue'),
      meta: {
        title: '编辑会议',
        keepAliveRoutes: ['org'],
      },
    },
    {
      path: '/meeting/detail',
      name: 'meeting-detail',
      component: () => import('@/views/meeting/detail.vue'),
      meta: {
        title: '会议详情',
      },
    },
    {
      path:'/meeting/sign',
      name:'meeting-sign',
      component:()=>import('@/views/meeting/sign.vue'),
      keepAlive: false
    },
    {
      path:'/meeting/select',
      name:'meeting-select',
      component:()=>import('@/views/select/index.vue'),
      keepAlive: false
    },
    {
      path: '/org',
      name: 'org',
      component: () => import('@/views/org/index.vue'),
      meta: {
        title: '组织成员',
        keepAliveRoutes: ['meeting-new', 'meeting-edit'], // 缓存的路径
      },
    },
  ],
})

export default routers
