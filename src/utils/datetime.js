import Type from './type'

const utils = {
  getRange: function(start, end, step = 1) {
    if (start > end) {
      throw Error('`start` greater than `end`')
    }
    let result = []
    while (start <= end) {
      result.push(start)
      start += step
    }
    return result
  },
  getStringX: function(x, length = 2) {
    let string_x = String(x)
    let i = string_x.length
    while (i < length) {
      string_x = '0' + string_x
      i += 1
    }
    return string_x
  },
}

class DateTime {
  constructor(datetime_or_timestamp) {
    // 兼容ios移动端， 把 2019-01-01 改成 2019/01/01
    if (Type.isString(datetime_or_timestamp)) {
      datetime_or_timestamp = datetime_or_timestamp.replace(/-/g, '/')
    }
    this.date = new Date(datetime_or_timestamp)
  }

  getYear() {
    return utils.getStringX(this.date.getFullYear())
  }

  getMonth() {
    return utils.getStringX(this.date.getMonth() + 1)
  }

  getDay() {
    return utils.getStringX(this.date.getDate())
  }

  getDate() {
    return this.getYear() + '-' + this.getMonth() + '-' + this.getDay()
  }

  getWeek() {
    let week = this.date.getDay()
    week = week === 0 ? 7 : week
    return week
  }

  getHour() {
    return utils.getStringX(this.date.getHours())
  }

  getMinute() {
    return utils.getStringX(this.date.getMinutes())
  }

  getTime() {
    return this.getHour() + ':' + this.getMinute()
  }

  addMonth() {
    let year = Number(this.getYear())
    let month = Number(this.getMonth())
    let day = this.getDay()
    if (month === 12) {
      year += 1
      month = 1
    } else {
      month += 1
    }
    month = utils.getStringX(month)
    return new DateTime(`${year}-${month}-${day}`)
  }

  subMonth() {
    let year = Number(this.getYear())
    let month = Number(this.getMonth())
    let day = this.getDay()
    if (month === 1) {
      year -= 1
      month = 12
    } else {
      month -= 1
    }
    month = utils.getStringX(month)
    return new DateTime(`${year}-${month}-${day}`)
  }

  addHour(n) {
    if (n < 0) {
      n = Math.floor(Math.abs(n))
    }
    let delta = n * 3600 * 1000
    return new DateTime(this.date.getTime() + delta)
  }

  addMinute(n) {
    if (n < 0) {
      n = Math.floor(Math.abs(n))
    }
    let delta = n * 60 * 1000
    return new DateTime(this.date.getTime() + delta)
  }

  toString() {
    return this.getDate() + ' ' + this.getTime()
  }

  format(fmt) {
    let date = fmt
    if (/YYYY/.test(fmt)) {
      date = date.replace('YYYY', this.getYear())
    }
    if (/YY/.test(fmt)) {
      date = date.replace('YY', this.getYear().substr(2))
    }

    if (/MM/.test(fmt)) {
      date = date.replace('MM', this.getMonth())
    }

    if (/DD/.test(fmt)) {
      date = date.replace('DD', this.getDay())
    }

    if (/HH/.test(fmt)) {
      date = date.replace('HH', this.getHour())
    }

    if (/mm/.test(fmt)) {
      date = date.replace('mm', this.getMinute())
    }
    return date
  }
}

const datetime = (function() {
  /**
   * 判断是否是闰年
   */
  const isLeapYear = year => {
    return year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)
  }

  /**
   * 30天的月份
   */
  const isShortMonth = month => {
    return [4, 6, 9, 11].includes(month)
  }

  /**
   * 根据年， 月获取最后一天
   */
  const getMonthEndDay = (year, month) => {
    if (isShortMonth(month)) {
      return 30
    } else if (month === 2) {
      return isLeapYear(year) ? 29 : 28
    } else {
      return 31
    }
  }

  /**
   * 获取年范围
   */
  const getYearRange = (startYear, endYear) => {
    return utils.getRange(startYear, endYear)
  }

  /**
   * 获取月份范围
   */
  const getMonthRange = (startMonth, endMonth) => {
    return utils.getRange(startMonth, endMonth)
  }

  /**
   * 获取月份列表
   */
  const getMonthList = () => {
    return getMonthRange(1, 12)
  }

  /**
   * 根据年，月获取日期列表
   */
  const getDayList = (year, month) => {
    return utils.getRange(1, getMonthEndDay(year, month))
  }

  /**
   * 根据基姆拉尔森计算公式, 某一天是星期几
   */
  const getWeek = (year, month, day) => {
    if (month === 1 || month === 2) {
      month += 12
      year -= 1
    }

    return (
      (day +
        2 * month +
        Math.floor((3 * (month + 1)) / 5) +
        year +
        Math.floor(year / 4) -
        Math.floor(year / 100) +
        Math.floor(year / 400) +
        1) %
      7
    )
  }

  const newDate = datetime_or_timestamp => {
    return new DateTime(datetime_or_timestamp)
  }

  /**
   * 获取今天的日期
   */
  const getToday = (divider = '-') => {
    let date = new Date()
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    return (
      year + divider + utils.getStringX(month) + divider + utils.getStringX(day)
    )
  }

  const getNowTime = () => {
    let date = new Date()
    let hour = date.getHours()
    let minute = date.getMinutes()
    return utils.getStringX(hour) + ':' + utils.getStringX(minute)
  }

  /**
   * 获取当前时间
   */
  const getNow = () => {
    return getToday() + ' ' + getNowTime()
  }

  /**
   * 显示时，分 列表
   * @param {*} interval 显示的分钟间隔, 7:00, 7:30
   */
  const getHourMinuteList = (startHour, endHour, interval = 30) => {
    let result = []
    while (startHour <= endHour) {
      result.push(startHour + ':' + '00')
      result.push(startHour + ':' + interval)
      startHour += 1
    }
    return result
  }

  return {
    isLeapYear,
    getYearRange,
    getMonthRange,
    getMonthList,
    getMonthEndDay,
    getDayList,
    getWeek,
    getHourMinuteList,
    getStringX: utils.getStringX,
    getToday,
    getNow,
    getNowTime,
    newDate,
  }
})()

export default datetime
