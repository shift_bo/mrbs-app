/**
 * 防抖
 * @param {Function} fn 执行的函数
 * @param {time} delay 延迟的时间
 */
export const debounce = (fn, delay) => {
  return function(args) {
    let self = this
    clearTimeout(fn.id)
    fn.id = setTimeout(function() {
      fn.call(self, args)
    }, delay)
  }
}

/**
 * 节流
 * @param {Function} fn 执行的函数
 * @param {time} delay 延迟的时间
 */
export const throttle = (fn, delay) => {
  let last, deferTimer
  return function(args) {
    let self = this
    let now = +new Date()
    if (last && now < last + delay) {
      clearTimeout(deferTimer)
      deferTimer = setTimeout(function() {
        last = now
        fn.call(self, args)
      }, delay)
    } else {
      last = now
      fn.call(self, args)
    }
  }
}

/**
 * 通过UA判断设备
 */
export const isIPhone = () => {
  return /iPhone/.test(navigator.userAgent)
}

export const isAndroid = () => {
  return /Android/.test(navigator.userAgent)
}
