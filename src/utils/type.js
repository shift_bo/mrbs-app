/**
 * 类型判断模块
 */
const type = (function() {
  const __ = arg => {
    return Object.prototype.toString.call(arg)
  }

  const isString = arg => {
    return __(arg) === '[object String]'
  }

  const isNumber = arg => {
    return __(arg) === '[object Number]'
  }

  const isBoolean = arg => {
    return __(arg) === '[object Boolean]'
  }

  const isObject = arg => {
    return __(arg) === '[object Object]'
  }

  const isFunction = arg => {
    return __(arg) === '[object Function]'
  }

  const isArray = arg => {
    return __(arg) === '[object Array]'
  }

  const isNull = arg => {
    return __(arg) === '[object Null]'
  }

  const isUndefined = arg => {
    return __(arg) === '[object Undefined]'
  }

  const isDate = arg => {
    return __(arg) === '[object Date]'
  }

  return {
    isString,
    isNumber,
    isBoolean,
    isObject,
    isFunction,
    isArray,
    isNull,
    isUndefined,
    isDate,
  }
})()

export default type
