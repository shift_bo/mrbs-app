import Type from './type'

/**
 * 生成缓存的时间戳
 * @param {Time} seconds 缓存的时间，秒数
 */
const makeExpiredTimeStamp = seconds => {
  return new Date().getTime() + seconds * 1000
}

/**
 * 设置
 * @param {String} key 缓存的键
 * @param {[String, Object]} value 缓存的值
 * @param {Number} expires 缓存的时间， 接受秒
 */
const set = (key, value, expires = null) => {
  if (Type.isArray(value) || Type.isObject(value)) {
    value = JSON.stringify(value)
  } else if (Type.isNull(value) || Type.isUndefined(value)) {
    value = ''
  } else if (Type.isFunction(value)) {
    throw new Error('TypeError: value type is Founction.')
  }
  localStorage.setItem(key, value)
  if (Type.isNumber(expires)) {
    let expiredKey = `${key}Expired`
    let expiredTime = makeExpiredTimeStamp(expires)
    localStorage.setItem(expiredKey, expiredTime)
  }
}

const existsKey = key => {
  let value = localStorage.getItem(key)
  if (value) {
    return true
  } else {
    return false
  }
}

/**
 * 判断是否存在有效期
 */
const existsExpires = key => {
  let expiredKey = `${key}Expired`
  let expiredTimeValue = localStorage.getItem(expiredKey)
  if (expiredTimeValue) {
    return true
  } else {
    return false
  }
}

/**
 * 存在
 * @param {String} key 缓存的键
 * @param {Boolean} remember 是否获取过期的缓存
 */
const exists = (key, remember = false) => {
  if (!existsKey(key)) {
    return false
  }

  if (!existsExpires(key)) {
    return true
  } else {
    let expiredKey = `${key}Expired`
    let expiredTimeValue = localStorage.getItem(expiredKey)
    let currentTimestamp = new Date().getTime()
    if (expiredTimeValue > currentTimestamp || remember === true) {
      return true
    } else {
      return false
    }
  }
}

/**
 * 获取
 * @param {String} key 缓存的键
 * @param {Boolean} remember 是否获取过期的缓存
 * @param {String} type 返回类型, 默认为string， 其他类型是返回
 */
const get = (key, remember = false, isParse = false) => {
  if (!exists(key, remember)) {
    return ''
  }
  let value = localStorage.getItem(key)
  if (isParse) {
    value = JSON.parse(value)
  }
  return value
}

/**
 * 删除
 * @param {String} key
 */
const remove = key => {
  if (existsKey(key)) {
    localStorage.removeItem(key)
    if (existsExpires(key)) {
      let expiredKey = `${key}Expired`
      localStorage.removeItem(expiredKey)
    }
  }
}

const clear = () => {
  localStorage.clear()
}

export default {
  set,
  get,
  exists,
  remove,
  clear,
}
