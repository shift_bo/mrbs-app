const getters = {
  areaList: state => state.app.areaList,
  employeeList: state => state.app.employeeList,
  selectEmployee: state => state.app.selectEmployee,
  currentUser: state => state.app.currentUser,
  routeStack: state => state.app.routeStack,
  selectedOrgs: state => state.app.selectedOrgs,
}

export default getters
