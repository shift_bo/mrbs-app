import { getAreaList, getCurrentUser } from '@/api'

const store = {
  state: {
    currentUser: null, // 当前用户
    routeStack: [], // 缓存的路由栈， 清除缓存需要将栈中所有的vm消除
    areaList: [], // 区域列表
    selectedOrgs: [], // 已选择的组织架构
  },
  mutations: {
    SET_CURRENT_USER(state, currentUser) {
      state.currentUser = currentUser
    },
    SET_AREA_LIST(state, areaList) {
      // 设置区域列表
      state.areaList = areaList
    },
    PUSH_ROUTE_STACK(state, vm) {
      let stack = state.routeStack
      if (!stack.includes(vm)) {
        state.routeStack.push(vm)
      }
    },
    DELETE_ROUTE_STACK(state, vm) {
      let stack = state.routeStack
      let idx = stack.indexOf(vm)
      if (idx > -1) {
        stack.splice(idx, 1)
        vm.$destroy()
      }
    },
    CLEAR_ROUTE_STACK(state) {
      // 消除缓存的路由组件
      let stack = state.routeStack
      for (let item of stack) {
        item.$destroy()
      }
      state.routeStack = []
    },
    SET_SELECT_ORG(state, org) {
      state.selectedOrgs = org
    },
    CLEAR_SELECT_ORG(state) {
      state.selectedOrgs = []
    },
  },
  actions: {
    // 异步获取区域列表
    ActionGetAreaList({ commit }) {
      return new Promise(resolve => {
        getAreaList().then(resp => {
          if (resp.status === 200) {
            const data = resp.data
            commit('SET_AREA_LIST', data.areaList)
            resolve(data)
          }
        })
      })
    },
    // 异步获取当前用户
    ActionGetCurrentUser({ commit }) {
      return new Promise(resolve => {
        getCurrentUser().then(resp => {
          if (resp.status === 200) {
            const data = resp.data
            commit('SET_CURRENT_USER', data.currentUser)
            resolve(data)
          }
        })
      })
    },
  },
}

export default store
