// 全局自定义指令

const directives = {
  focus: {
    inserted: function(el) {
      el.focus()
    },
  },

  scroll: function(el, binding) {
    const direction = binding.arg || 'x'
    const value = binding.value || 960
    if (direction === 'x') {
      el.style.overflowX = 'scroll'
      el.style.maxWidth = value + 'px'
    } else {
      el.style.overflowY = 'scroll'
      el.style.maxHeight = value + 'px'
    }
  },
}

export default directives
