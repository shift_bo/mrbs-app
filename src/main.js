import Vue from 'vue'
import App from './App.vue'
import 'normalize.css/normalize.css'
import './style/main.css'
import './assets/fonts/iconfont.css'
import router from './router'
import store from './store'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import MPlugin from '@/plugin'
import { Popover } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Vant from 'vant';
import 'vant/lib/index.css';

Vue.use(Vant)
Vue.use(MintUI)
Vue.use(MPlugin)
Vue.use(Popover)
console.log(1)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
